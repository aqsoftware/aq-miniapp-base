// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import type {
  MiniAppCredentials
} from 'aq-miniapp-core';
import {
  MiniApp
} from 'aq-miniapp-react';
import View from './views/View';
import Create from './views/Create';
import './index.css';

const credentials: MiniAppCredentials = {
  id: '',
  key: ''
}

ReactDOM.render(
  <MiniApp
    credentials={credentials}
    create={Create}
    join={View}
    data={{}}
    devt={true}
  />,
  document.getElementById('root')
);
