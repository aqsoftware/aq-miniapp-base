// @flow
import React, { Component } from 'react';
import './Progress.css';

type Props = {
}

export default class Progress extends Component<Props> {

  static defaultProps = {};

  render() {
    return (
      <div className="loading">
        <div className="loading-bar"></div>
        <div className="loading-bar"></div>
        <div className="loading-bar"></div>
        <div className="loading-bar"></div>
      </div>
    );
  }
}