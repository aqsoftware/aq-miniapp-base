// @flow
import React, { Component } from 'react';
import Preload from 'react-preload';
import Progress from '../components/Progress';
import {
  CloudStorage,
  defaultLifeCycle
} from 'aq-miniapp-core';
import {
  StaticCanvas,
  Background
} from 'aq-miniapp-react';
import {
  View1,
  View2,
  View3,
} from './js';
import Assets, { ASSETS } from '../assets';

import type { Output } from './Types';

type Props = {
  cloudStorageClient: CloudStorage,
  id?: string,
  data? : Object,
  mode: 'preview' | 'join'
}

type State = {
  currentPage: number,
  output: Output,
  data: ?Object,
  mode: 'preview' | 'join',
}

export default class View extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      currentPage: 1,
      output: {},
      data: props.data,
      mode: props.mode
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    // if(!nextProps.id) {
    //   this.setState({data: nextProps.data, currentPage: 1});
    // }
  }

  _didPreload() {
    console.log(`Preloaded`)
  }

  _onPreloadError(err: any) {
    console.log(`Preloading error= ${err}`)
  }

  _onView1Click() {
    this.setState({currentPage: 2});
  }

  _onView2Click(output: Output) {
    this.setState({currentPage: 3, output: output});
  }

  _onView3Click() {
    this.setState({currentPage: 1});
    defaultLifeCycle.end();
  }

  render() {
    // const data = this.state.data;
    const height = window.innerWidth;
    const width = window.innerHeight;
    let render = <StaticCanvas width={width} height={height}/>

    // if (data) {
      // let source = data.source;
      // if (this.props.additionalInfo && this.props.additionalInfo.passSource) {
      //   source = this.props.additionalInfo.passSource;
      // }
      switch (this.state.currentPage) {
        case 1:
          render = <View1 onClick={this._onView1Click.bind(this)}/>
          break;
        case 2:
          render = <View2 onClick={this._onView2Click.bind(this)}/>
          break;
        case 3:
          render = <View3 output={this.state.output} onClick={this._onView3Click.bind(this)}/>
          break;
        default:
          break;
      }
    // }
    return (
      <Preload
        loadingIndicator={<Progress/>}
        images={ASSETS}
        resolveOnError={true}
        mountChildren={true}
        onSuccess={this._didPreload.bind(this)}
        onError={this._onPreloadError.bind(this)}
      >
        <div className="container">
          <Background
            image={Assets.images.background}
          />
          {render}
        </div>
      </Preload>
    );
  }
}
