import React, { Component } from 'react';
import type { Output } from '../Types';

//Import relevant components as required by specs document here
import { Button } from 'aq-miniapp-react';

/* Import Assets as required by specs document
ex.
import asset from '../../assets/asset.png';
*/

// Import CSS here
import '../css/View3.css';

type Props = {
  output: Output
}

type State = {}

export default class View3 extends Component<Props, State> {
  
  render() {
    return (
      <div className="viewContainer justifyCenter">
        <div className="title">Done</div>
        <Button title="Restart" onClick={this.props.onClick}/>
      </div>
    )
  }
}
