// @flow
import React, { Component } from 'react';

//Import relevant components as required by specs document here
import { Button } from 'aq-miniapp-react';

/* Import Assets as required by specs document
ex.
import asset from '../../assets/asset.png';
*/

// Import CSS here
import '../css/View.css';

type Props = {
  onClick: () => void
}
type State = {}

export default class View1 extends Component<Props, State> {
  render() {
    return (
      <div className="viewContainer justifySpaceAround">
        <Button title="Start" onClick={this.props.onClick}/>
      </div>
    )
  }
}
