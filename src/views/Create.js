// @flow
import React, { Component } from 'react';
import {
  CloudStorage,
  defaultLifeCycle,
  defaultUIBridge
} from 'aq-miniapp-core';
import {
  StaticCanvas,
  Button,
  Panel
} from 'aq-miniapp-react';
import './css/Create.css';

const TITLE = "Title";

type Props = {
  cloudStorageClient: CloudStorage,
  data: Object
}

type State = {
  coverImg: string
}
export default class Create extends Component<Props, State> {

  componentWillMount() {
    // Set callback function to be called when AQ app requests to preview the
    // current content of our create screen
    defaultLifeCycle.setRequestPreviewCallback(this._showPreview.bind(this));
    defaultLifeCycle.setPublishCallback(this._publish.bind(this));
  }

  _publish(id: string) {
    this.props.cloudStorageClient.insert({
      id: id,
      title: TITLE,
      source: this.props.data.source
    })
      .then((response) => {
        defaultLifeCycle.publishSucceded();
      })
      .catch((error) => {
        console.log(`Error occured while sending to cloud storage: ${error}`);
        defaultLifeCycle.publishFailed();
      });
  }

  _showPreview() {
    // At the very least, AQ app requires a title and a cover image,
    // before the preview screen can be shown.
    defaultLifeCycle.showPreviewWithData(TITLE, this.state.coverImg, null);
  }

  _onButtonClick() {
    defaultUIBridge.showGalleryImageSelector('cover', 'Select a cover photo', this._onRequestCoverImage.bind(this));
    // defaultLifeCycle.showPreviewWithData(TITLE, COVER_IMAGE, null);
  }

  _onRequestCoverImage(key: string, coverImg: string) {
    this.setState({ coverImg: coverImg }, () => {
      this._showPreview();
    });
  }

  render() {
    const width = window.innerWidth;
    const height = window.innerHeight;
    return (
      <div className='container'>
        <StaticCanvas id="static" width={width} height={height} />
        <Panel
          id="content"
          title="Content"
          backgroundColor="rgba(38, 38, 38, 0.8)"
          width="200px"
          titleColor="White"
          className="uppercase lighter"
        >
          <Button title="Done" onClick={() => { this._onButtonClick(); }} />
        </Panel>
      </div>
    );
  }
}
