// @flow
import background from './images/background.jpg';

/* Define common assets here */
const Assets = {
  images: {
    background: background
  },
  sounds: {
  },
  fonts: {
  }
}

/* Array of common assets to be used by Hexi Loader */
export const ASSETS = [
  Assets.images.background
];

export default Assets;